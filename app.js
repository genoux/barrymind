var createError = require('http-errors'),
  express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  logger = require('morgan'),
  favicon = require('serve-favicon'),
  app = express(),
  pjson = require('./package.json'),
  fs = require("fs");

global.globalString = pjson.version;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use(express.static(__dirname + '/public'));
//app.use(express.static('public'));
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));


app.get("/test", function (req, res) {
  res.render('test.pug');
});

var indexRouter = require('./routes/index');
app.use('/', indexRouter);

app.set('app', pjson);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {
    url: req.url
  });
});

app.listen(8080, function () {
  console.log('Server running on port: ' + 8080)
})

module.exports = app;