var express = require("express");
var router = express.Router();
var fs = require("fs");
var async = require("async");
var sizeOf = require("image-size");
var url = {
  projects: "./data/projects.json",
  knowledge: "./data/knowledge.json"
};
var jsonData = [];

// const compress_images = require("compress-images")

// const INPUT_path_to_your_images = "public/images/projects/img/thumbnail/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";
// const OUTPUT_path = "public/images/projects/img/";

// compress_images(INPUT_path_to_your_images, OUTPUT_path, { compress_force: false, statistic: true, autoupdate: true }, false,
//                 { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
//                 { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
//                 { svg: { engine: "svgo", command: "--multipass" } },
//                 { gif: { engine: "gifsicle", command: ["--colors", "75", "--use-col=web"] } },
//   function (error, completed, statistic) {
//     console.log("-------------");
//     console.log(error);
//     console.log(completed);
//     console.log(statistic);
//     console.log("-------------");
//   }
// );

router.get('/version', function (req, res, next) {
  var content = fs.readFileSync("./package.json");
  res.json(JSON.parse(content));
});

/* GET home page. */
router.get("/", function (req, res, next) {
  async.forEachOf(
    url,
    (value, key, callback) => {
      fs.readFile(value, "utf8", (err, data) => {
        if (err) return callback(err);
        try {
          jsonData[key] = JSON.parse(data);
          jsonData["projects"].forEach(function (item, i) {
            var dimensions_src = sizeOf("./public/" + item.src);
            console.log('dimensions_src', dimensions_src);
            var dimensions_thumb = sizeOf("./public/" + item.thumb);
            item["dimensionsSrc"] = dimensions_src
            item["dimensionsThumb"] = dimensions_thumb
          });
        } catch (e) {
          return callback(e);
        }
        callback();
      });
    },
    err => {
      if (err) console.error(err.message);
      console.log("Done loading assets... ✌️");
      res.render("index", {
        app: req.app.get('app'),
        title: "Barrymind.",
        data: jsonData,
        listCategory: ['Tous','Développement Web','Image de marque','Conception visuelle','Projection Mapping', 'Autres', ]
      });
    }
  );
});
module.exports = router;