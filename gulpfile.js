var gulp = require("gulp"),
  nodemon = require("gulp-nodemon"),
  browserSync = require("browser-sync").create();
const concat = require("gulp-concat");
const terser = require("gulp-terser");
let cleanCSS = require("gulp-clean-css");
var sass = require('gulp-sass');
var browserify = require('gulp-browserify');
var rename = require("gulp-rename");
sass.compiler = require('node-sass');
// ------------------------------------------------- configs
var paths = [
  "./node_modules/bootstrap/dist/css/bootstrap.css",
  "./node_modules/photoswipe/dist/photoswipe.css",
  "./node_modules/aos/dist/aos.css",
  './public/stylesheets/sass/site.scss'
];

function browserSyncInit() {
  setTimeout(() => {
    browserSync.init({
      open: false,
      proxy: "http://localhost:8080/"
    });
  }, 500);
} //#endregion

function scssCompile() {
  return gulp
    .src(paths)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat("style.css"))
    .pipe(
      cleanCSS({
        compatibility: "ie8"
      })
    )
    .pipe(gulp.dest('./public/stylesheets/'))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
}

function watch() {
  console.log("Watching... *.*");
  gulp.watch('./public/stylesheets/sass/site/*.scss', gulp.parallel("scssCompile"));
  gulp.watch('./public/javascripts/*/*.js', gulp.parallel("jsCompile"));
  gulp.watch("./views/**/*.pug", browserSync.reload);
  //gulp.watch("./**/*.js", browserSync.reload);
}

function jsCompile() {
  return gulp.src('public/javascripts/core/main.js')
    .pipe(browserify())
    .pipe(rename('bundle.js'))
    .pipe(terser())
    .pipe(gulp.dest('public/javascripts/'))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
}

function server() {
  var stream = nodemon({
    script: "app.js",
    ignore: ['bundle.js', 'style.css'],
    tasks: ['server', 'scssCompile', 'jsCompile'],
    ext: 'pug js scss'
  })

  stream
    .on("restart", function () {
      scssCompile();
      jsCompile()
    })
    .on("start", function () {})
    .on("crash", function () {
      console.error("Application has crashed!\n");
      stream.emit("restart", 10); // restart the server in 10 seconds
    });
}

exports.server = server;
exports.watch = watch;
exports.scssCompile = scssCompile;
exports.jsCompile = jsCompile;
exports.browserSyncInit = browserSyncInit;

var build = gulp.parallel(scssCompile, jsCompile);
gulp.task('default', build);