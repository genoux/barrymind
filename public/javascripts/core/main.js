global.$ = require('jquery')
global.jQuery = require("jquery")
var AOS = require("aos"),
  SVGInjector = require("svg-injector"),
  anime = require('animejs/lib/anime'),
  Shuffle = require("shufflejs");
require("../library/pswp-init");
require("jquery-backstretch");
require("./version.js");

$(document).ready(function () {

  //
  // ─── Backstresh ───────────────────────────────────────────────────────────────────────────
  //
  var backstretchResolve = new Promise(function (resolve, reject) {
    $(".dataBg")
      .each(function () {
        $(this).backstretch($(this).data("bg"), {
          transitionDuration: 300
        });
      })
      .promise()
      .done(function (e) {
        $(".grid-item .content").each(function (index) {
          let randomHeight = Math.floor(Math.random() * 325 + 280);
          $(this).css("height", randomHeight + "px");
        }).promise().done(function () {
          resolve();
        })
      });
  });

  //
  // ─── AOS ───────────────────────────────────────────────────────────────────────────
  //  
  var aosDataResolve = new Promise(function (resolve, reject) {
    $(".aos")
      .each(function (item) {
        $(this).attr("data-aos", "fade-down");
      })
      .promise()
      .done(function () {
        resolve();
      });
  });

  //
  // ─── SVG INJECTOR ───────────────────────────────────────────────────────────────────────────
  // 
  var svgImageInjector = document.querySelectorAll(".svg-inject");
  var injectResolve = new Promise(function (resolve, reject) {
    var injectorOptions = {
      evalScripts: 'once',
      pngFallback: 'public/images/res/logo.svg',
      each: function (svg) {
        $(svg).css("visibility", "visible");
      }
    };
    SVGInjector(svgImageInjector, injectorOptions, function () {
      resolve();
    });
  });

  //
  // ───────────────────────────────────────────────────────────────────────── PROMISE RESOLVE ─────
  //
  Promise.all([backstretchResolve, aosDataResolve, injectResolve]).then(() => {
    console.log("Page done...");
    BrandLogoAnimation();
    $('.loader').fadeOut('fast');
    $('.elements').css('opacity', 1);
  });

  //
  // ─── Logo Animation ───────────────────────────────────────────────────────────────────────────
  //  
  function BrandLogoAnimation() {
    let animSpeed = 1000;
    //INIT TRANSITION PAGE
    var fillDrawing = anime({
      targets: ".animjs .a",
      opacity: 1,
      easing: "easeInOutSine",
      duration: animSpeed
    });
    var scaleAnim = anime({
      targets: ".logo",
      scale: [0, 1],
      easing: "easeInOutQuart",
      duration: animSpeed - 200,
      complete: function (anim) {
        AOS.init({
          duration: 800
        });
      }
    });
    var lineDrawing = anime({
      targets: ".animjs .b",
      strokeDashoffset: [anime.setDashoffset, 0],
      easing: "easeInOutSine",
      duration: animSpeed
    });
    var lineDrawing = anime({
      targets: ".animjs .c, .animjs .d",
      strokeDashoffset: [anime.setDashoffset, 0],
      easing: "easeInOutSine",
      duration: animSpeed
    });
  }


  $('body').on('click', '.logo', function () {
    BrandLogoAnimation();
  })
  //
  // ─── Scroll Animation Event ───────────────────────────────────────────────────────────────────────────
  //   
  $('a[href^="#"]').on("click", function (e) {
    e.preventDefault();
    var target = this.hash;
    $target = $(target);
    $("html, body")
      .stop()
      .animate({
          scrollTop: $target.offset().top + 50
        },
        500,
        "swing"
      );
  });


  //
  // ─── Shuffle Grid ───────────────────────────────────────────────────────────────────────────
  // 
  var element = document.querySelector('.shuffle-container');
  var sizer = element.querySelector('.my-sizer-element');
  var shuffleInstance = new Shuffle(element, {
    itemSelector: '.picture-item',
    sizer: sizer, // could also be a selector: '.my-sizer-element'
    buffer: 1,
  });


  //
  // ─── Filter Button ───────────────────────────────────────────────────────────────────────────
  //  
  var button = document.querySelectorAll(".filter-options button")
  $(button[0]).addClass("active").siblings().removeClass('active');
  $(button).on("click", function () {
    var elmData = $(this).data('group');
    if (elmData != 'Tous') {
      shuffleInstance.filter(elmData);
      $(this).addClass("active").siblings().removeClass('active');
    } else {
      $(this).addClass("active").siblings().removeClass('active');
      shuffleInstance.filter();
    }
  });
});